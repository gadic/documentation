# Dongjun Wu <ziyawu@gmail.com>, 2012, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Tracing\n"
"POT-Creation-Date: 2020-04-28 20:51+0200\n"
"PO-Revision-Date: 2016-07-22 07:57+0800\n"
"Last-Translator: Dongjun Wu <ziyawu@gmail.com>\n"
"Language-Team: Chinese <zh-l10n@linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Chinese (traditional)\n"
"X-Poedit-Country: TAIWAN\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: tracing-f06.svg:49(format) tracing-f05.svg:49(format)
#: tracing-f04.svg:49(format) tracing-f03.svg:49(format)
#: tracing-f02.svg:49(format) tracing-f01.svg:49(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#: tracing-f06.svg:70(tspan) tracing-f05.svg:70(tspan)
#: tracing-f04.svg:70(tspan) tracing-f03.svg:70(tspan)
#: tracing-f02.svg:70(tspan)
#, no-wrap
msgid "Original Image"
msgstr "原始圖像"

#: tracing-f06.svg:81(tspan)
#, no-wrap
msgid "Traced Image / Output Path - Simplified"
msgstr "描繪圖像 / 輸出路徑 - 已簡化"

#: tracing-f06.svg:86(tspan)
#, no-wrap
msgid "(384 nodes)"
msgstr "(384 個節點)"

#: tracing-f05.svg:81(tspan)
#, no-wrap
msgid "Traced Image / Output Path"
msgstr "描繪圖像 / 輸出路徑"

#: tracing-f05.svg:86(tspan)
#, no-wrap
msgid "(1,551 nodes)"
msgstr "(1,551 個節點)"

#: tracing-f04.svg:81(tspan) tracing-f04.svg:97(tspan)
#, no-wrap
msgid "Quantization (12 colors)"
msgstr "量化 (12 種顏色)"

#: tracing-f04.svg:86(tspan) tracing-f03.svg:86(tspan)
#: tracing-f02.svg:86(tspan)
#, no-wrap
msgid "Fill, no Stroke"
msgstr "填色，無邊框"

#: tracing-f04.svg:102(tspan) tracing-f03.svg:102(tspan)
#: tracing-f02.svg:102(tspan)
#, no-wrap
msgid "Stroke, no Fill"
msgstr "邊框，無填色"

#: tracing-f03.svg:81(tspan) tracing-f03.svg:97(tspan)
#, no-wrap
msgid "Edge Detected"
msgstr "偵測邊緣"

#: tracing-f02.svg:81(tspan) tracing-f02.svg:97(tspan)
#, no-wrap
msgid "Brightness Threshold"
msgstr "亮度臨界值"

#: tracing-f01.svg:70(tspan)
#, no-wrap
msgid "Main options within the Trace dialog"
msgstr "描繪對話窗的主要選項"

#: tutorial-tracing.xml:7(title)
msgid "Tracing bitmaps"
msgstr "描繪點陣圖"

#: tutorial-tracing.xml:8(subtitle)
msgid "Tutorial"
msgstr ""

#: tutorial-tracing.xml:13(para)
msgid ""
"One of the features in Inkscape is a tool for tracing a bitmap image into a "
"&lt;path&gt; element for your SVG drawing. These short notes should help you "
"become acquainted with how it works."
msgstr ""
"Inkscape 的特點之一就是有工具能將點陣圖描繪成 &lt;路徑&gt; 元件作為 SVG 圖"
"畫。這些簡短的說明應該能幫助你熟悉它的用法。"

#: tutorial-tracing.xml:20(para)
msgid ""
"Currently Inkscape employs the Potrace bitmap tracing engine (<ulink url="
"\"http://potrace.sourceforge.net\">potrace.sourceforge.net</ulink>) by Peter "
"Selinger. In the future we expect to allow alternate tracing programs; for "
"now, however, this fine tool is more than sufficient for our needs."
msgstr ""
"目前 Inkscape 採用 Potrace 點陣圖描繪引擎 (<ulink url=\"http://potrace."
"sourceforge.net\">potrace.sourceforge.net</ulink>) 其作者為 Peter Selinger。"
"我們期望將來有候補的描繪程式；不過，現在這個優秀的工具已經能大大滿足我們的需"
"求。"

#: tutorial-tracing.xml:27(para)
msgid ""
"Keep in mind that the Tracer's purpose is not to reproduce an exact "
"duplicate of the original image; nor is it intended to produce a final "
"product. No autotracer can do that. What it does is give you a set of curves "
"which you can use as a resource for your drawing."
msgstr ""
"記住描繪的目的並非仿造出原始圖像的精確複製品；也不是刻意打造一個最終作品。沒"
"有自動描繪程式可以做到那樣。它能做的是給你一組曲線作為素材應用到你的創作中。"

#: tutorial-tracing.xml:34(para)
msgid ""
"Potrace interprets a black and white bitmap, and produces a set of curves. "
"For Potrace, we currently have three types of input filters to convert from "
"the raw image to something that Potrace can use."
msgstr ""
"Potrace 可詮釋一幅黑白點陣圖並產生一組曲線。目前我們有三種類型的輸入濾鏡可將"
"原本圖像轉換成 Potrace 可使用的東西。"

#: tutorial-tracing.xml:40(para)
msgid ""
"Generally the more dark pixels in the intermediate bitmap, the more tracing "
"that Potrace will perform. As the amount of tracing increases, more CPU time "
"will be required, and the &lt;path&gt; element will become much larger. It "
"is suggested that the user experiment with lighter intermediate images "
"first, getting gradually darker to get the desired proportion and complexity "
"of the output path."
msgstr ""
"一般來說中間點陣圖中含有較多的暗色像素， Potrace 會實行更多次的描繪動作。當描"
"繪的次數增加時，會需要更多的 CPU 時間，且產生的 &lt;路徑&gt; 元件也會大很多。"
"建議使用者先試驗顏色較亮的中間點陣圖，再慢慢地換成顏色較暗的以得到想要的輸出"
"路徑比例和複雜性。"

#: tutorial-tracing.xml:48(para)
#, fuzzy
msgid ""
"To use the tracer, load or import an image, select it, and select the "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Trace Bitmap</guimenuitem></"
"menuchoice> item, or <keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"alt\">Alt</keycap><keycap>B</keycap></keycombo>."
msgstr ""
"開始使用描繪功能，載入或匯入一個圖像，選取它並選擇 <command>路徑 &gt; 描繪點"
"陣圖</command> 項目，或者直接按 <keycap>Shift+Alt+B</keycap>。"

#: tutorial-tracing.xml:60(para)
msgid "The user will see the three filter options available:"
msgstr "這時會看到三種可用的濾鏡選項："

#: tutorial-tracing.xml:65(para)
msgid "Brightness Cutoff"
msgstr "亮度界限值"

#: tutorial-tracing.xml:70(para)
msgid ""
"This merely uses the sum of the red, green and blue (or shades of gray) of a "
"pixel as an indicator of whether it should be considered black or white. The "
"threshold can be set from 0.0 (black) to 1.0 (white). The higher the "
"threshold setting, the fewer the number pixels that will be considered to be "
"“white”, and the intermediate image with become darker."
msgstr ""
"這個僅僅使用像素的紅色、綠色、藍色 (或灰度) 的總和作為判定哪一個為黑或白的指"
"標。臨界值可設定為 0.0 (黑色) 到 1.0 (白色)。設定較高的臨界值，像素數量少於臨"
"界值的會判定為白色，而中間影像會變得較暗。"

#: tutorial-tracing.xml:86(para)
msgid "Edge Detection"
msgstr "邊緣偵測"

#: tutorial-tracing.xml:91(para)
msgid ""
"This uses the edge detection algorithm devised by J. Canny as a way of "
"quickly finding isoclines of similar contrast. This will produce an "
"intermediate bitmap that will look less like the original image than does "
"the result of Brightness Threshold, but will likely provide curve "
"information that would otherwise be ignored. The threshold setting here (0.0 "
"– 1.0) adjusts the brightness threshold of whether a pixel adjacent to a "
"contrast edge will be included in the output. This setting can adjust the "
"darkness or thickness of the edge in the output."
msgstr ""
"這個使用 J. Canny 發明的邊緣偵測算法作為快速發現相似對比的等斜線的方法。這個"
"產生的中間點陣圖看起來比使用亮度臨界值的效果還不像原始圖像，但是很可能會提供"
"另一個方式被忽略的曲線資訊。這裡設定的臨界值 (0.0 – 1.0) 可調整輸出結果中相鄰"
"像素是否達到邊緣差異的亮度臨界值。這個設定能調整輸出結果中邊緣的明暗或粗細。"

#: tutorial-tracing.xml:109(para)
msgid "Color Quantization"
msgstr "顏色量化"

#: tutorial-tracing.xml:114(para)
msgid ""
"The result of this filter will produce an intermediate image that is very "
"different from the other two, but is very useful indeed. Instead of showing "
"isoclines of brightness or contrast, this will find edges where colors "
"change, even at equal brightness and contrast. The setting here, Number of "
"Colors, decides how many output colors there would be if the intermediate "
"bitmap were in color. It then decides black/white on whether the color has "
"an even or odd index."
msgstr ""
"這個濾鏡的效果會產生一個不同於其他兩種濾鏡的中間影像，但是非常有用。這個濾鏡"
"會尋找顏色變化的邊緣即使在同等亮度和對比下，而不是顯示亮度或對比的等斜線。如"
"果中間點陣圖是彩色的，顏色數目的設定值會決定有多少種輸出顏色。還會決定黑色/白"
"色是否有偶數或奇數索引。"

#: tutorial-tracing.xml:130(para)
msgid ""
"The user should try all three filters, and observe the different types of "
"output for different types of input images. There will always be an image "
"where one works better than the others."
msgstr ""
"使用者應該嘗試全部三種濾鏡，並且觀察對於不同類型的輸入圖像產生效果的差異。總"
"有某些圖片用其中一種濾鏡的效果比其他兩種好。"

#: tutorial-tracing.xml:136(para)
#, fuzzy
msgid ""
"After tracing, it is also suggested that the user try "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Simplify</guimenuitem></"
"menuchoice> (<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</"
"keycap></keycombo>) on the output path to reduce the number of nodes. This "
"can make the output of Potrace much easier to edit. For example, here is a "
"typical tracing of the Old Man Playing Guitar:"
msgstr ""
"描繪後，建議使用者在輸出路徑上試著用 <command>路徑 &gt; 簡化</command> "
"(<keycap>Ctrl+L</keycap>) 以減少節點數。這樣會讓 Potrace 的輸出結果更容易編"
"輯。例如，下面有一幅「老人彈吉他」的典型描繪："

#: tutorial-tracing.xml:150(para)
#, fuzzy
msgid ""
"Note the enormous number of nodes in the path. After hitting "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</keycap></"
"keycombo>, this is a typical result:"
msgstr ""
"注意路徑中有龐大的節點數量。按 <keycap>Ctrl+L</keycap> 之後，這是典型的效果："

#: tutorial-tracing.xml:162(para)
msgid ""
"The representation is a bit more approximate and rough, but the drawing is "
"much simpler and easier to edit. Keep in mind that what you want is not an "
"exact rendering of the image, but a set of curves that you can use in your "
"drawing."
msgstr ""
"這表示有點近似和粗造，但是圖畫愈簡單愈容易編輯。記住你得到的不是圖像的精確描"
"寫，但你可以使用這一組曲線到你的繪畫中。"

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-tracing.xml:0(None)
msgid "translator-credits"
msgstr "Dong-Jun Wu <ziyawu@gmail.com>, 2009, 2014, 2016"

#~ msgid "Optimal Edge Detection"
#~ msgstr "最佳邊緣偵測"
