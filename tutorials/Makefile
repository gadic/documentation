# Makefile written by
# Colin Marquardt <colin@marquardt-home.de>, 2007.

ALL_TUTORIALS += advanced
ALL_TUTORIALS += basic
ALL_TUTORIALS += calligraphy
ALL_TUTORIALS += elements
ALL_TUTORIALS += interpolate
ALL_TUTORIALS += shapes
ALL_TUTORIALS += tips
ALL_TUTORIALS += tracing
ALL_TUTORIALS += tracing-pixelart

# The targets here go through all tutorial directories and see if make can be run there
# The Makefile for each tutorial will find out about the languages it can generate.

.PHONY: all
all:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && if [ -r Makefile ]; then $(MAKE) all; fi); \
	done

.PHONY: update-authors
update-authors:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && if [ -r Makefile ]; then $(MAKE) update-authors; fi); \
	done

.PHONY: html
html:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && if [ -r Makefile ]; then $(MAKE) html; fi); \
	done

.PHONY: svg
svg:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && if [ -r Makefile ]; then $(MAKE) svg; fi); \
	done

.PHONY: pot
pot:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && if [ -r Makefile ]; then $(MAKE) pot; fi); \
	done

.PHONY: po
po:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && if [ -r Makefile ]; then $(MAKE) po; fi); \
	done

.PHONY: images
images:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && if [ -r Makefile ]; then $(MAKE) images MYLANG=$(MYLANG); fi); \
	done

.PHONY: allimages
allimages:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && if [ -r Makefile ]; then $(MAKE) allimages; fi); \
	done

.PHONY: check
check:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && if [ -r Makefile ]; then $(MAKE) check MYLANG=$(MYLANG); fi); \
	done

.PHONY: copy
copy: copy-svg copy-html

.PHONY: copy-html
copy-html:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && if [ -r Makefile ]; then $(MAKE) copy-html; fi); \
	done

.PHONY: copy-svg
copy-svg:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && if [ -r Makefile ]; then $(MAKE) copy-svg; fi); \
	done

.PHONY: clean
clean:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && if [ -r Makefile ]; then $(MAKE) clean; fi); \
	done

.PHONY: clean-export
clean-export: clean-html-export clean-svg-export

.PHONY: clean-html-export
clean-html-export:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && if [ -r Makefile ]; then $(MAKE) clean-html-export; fi); \
	done

.PHONY: clean-svg-export
clean-svg-export:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && if [ -r Makefile ]; then $(MAKE) clean-svg-export; fi); \
	done

.PHONY: clean-html
clean-html:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && if [ -r Makefile ]; then $(MAKE) clean-html; fi); \
	done

.PHONY: clean-svg
clean-svg:
	@for TUTORIAL in $(ALL_TUTORIALS); do \
		(cd $${TUTORIAL} && if [ -r Makefile ]; then $(MAKE) clean-svg; fi); \
	done

.PHONY: help
help:
	@$(MAKE) --no-print-directory -f Makefile.targets help
